﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class QuestionManagerSubjective : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField]
    private TMP_Text question;

    [SerializeField]
    private Image questionImage;

    [SerializeField]
    private TMP_InputField userAnswer;

    [SerializeField]
    private ScoreSystem scoreSystem;

    [SerializeField]
    private int point;
    
    [SerializeField]
    private QuestionDataSubjective[] questionsArray;

    private string userAns;
    private string correctAns;
    private int index;
    
    void Start()
    {
        //the index can be based on the trigger on the tiles.
        //question.SetText(questionsArray[0]._description);
        //userAnswer = gameObject.GetComponent<TMP_InputField>();
        //setQuestion(1);
    }

    public void setQuestion(){
        
        //TODO Implement a for loop to search and compare the id for the questions.
        questionImage.sprite = questionsArray[index]._questionImage;
        question.SetText(questionsArray[index]._description);
        correctAns = questionsArray[index]._answer;
    }

    public void checkAnswer(){
        if(userAns.Equals(correctAns)){
            Debug.Log("Correct");
            scoreSystem.updateScore(point);
            Debug.Log("Current score: " + scoreSystem._getScore);
        }
        else
        {
            Debug.Log("Wrong");
            Debug.Log("Current score: " + scoreSystem._getScore);
        }
    }

    public void getAnswer(){
        //TMP_InputField userAnswer = gameObject.GetComponent<TMP_InputField>();
        userAns = userAnswer.GetComponent<TMP_InputField>().text;
        Debug.Log("You wrote: " + userAns);
    }

    public void addQuestionIndex(){
        index = index + 1;
        Debug.Log("Index: " + index);
    }
}
