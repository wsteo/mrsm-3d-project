﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour
{
    [SerializeField]
    private TMP_Text timerText;

    [SerializeField]
    private TMP_Text timeUsedText;

    [SerializeField]
    private GameObject player;
    private static float timeRemaining = 3600f;
    private bool startTimer = false;

    private static float timeUsed;

    private void Awake() {
        StartTimer();
    }
    // Update is called once per frame
    void Update()
    {
        
        if (player.GetComponent<FollowWP>().current == 36)
        {
            StopTimer();
            DisplayTimeUsed();
        }
        

        if(startTimer){
            timeRemaining -= Time.deltaTime;    
        }
        
        DisplayTime();
        //DisplayTimeUsed();
    }

    public void DisplayTime(){
        float minutes = Mathf.FloorToInt(timeRemaining / 60);
        float seconds = Mathf.FloorToInt(timeRemaining % 60);
        float milliSeconds = (timeRemaining % 1) * 1000;
        timerText.SetText(string.Format("{0:00}:{1:00}:{2:000}", minutes, seconds, milliSeconds));
    }

    public void DisplayTimeUsed(){
        timeUsed = 3600 - timeRemaining;
        float minutes = Mathf.FloorToInt(timeUsed / 60);
        float seconds = Mathf.FloorToInt(timeUsed % 60);
        float milliSeconds = (timeUsed % 1) * 1000;
        timeUsedText.SetText(string.Format("{0:00}:{1:00}:{2:000}", minutes, seconds, milliSeconds));
    }

    public void StartTimer(){
        startTimer = true;
    }

    public void StopTimer(){
        startTimer = false;
    }
}
