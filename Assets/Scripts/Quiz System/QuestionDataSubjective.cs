using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "QuestionDataSubjective", menuName = "MRSM-Project-3D/QuestionDataSubjective", order = 0)]
public class QuestionDataSubjective : ScriptableObject {
    [SerializeField]
    private int id;
    
    [SerializeField]
    private Sprite questionImage;

    [SerializeField]
    [TextArea(3,10)]
    private string description;

    [SerializeField]
    [TextArea(3,10)]
    private string answer;

    public int _id{
        get{
            return id;
        }
    }

    public string _description{
        get{
            return description;
        }
    }

    public string _answer{
        get{
            return answer;
        }
    }

    public Sprite _questionImage{
        get{
            return questionImage;
        }
    }
}