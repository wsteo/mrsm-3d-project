﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Tile : MonoBehaviour
{
    //[SerializeField]
    //private QuestionDataSubjective questionData;

    private bool setQuestion = false;

    [SerializeField]
    private GameObject questionUI;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("Enter Area!");
            setQuestion = true;
            questionUI.SetActive(true);            
            if (setQuestion && m_SetQuestionEvent != null)
            {
                m_SetQuestionEvent.Invoke();
            }
        }

    }

    private void OnTriggerExit(Collider other)
    {

        if (other.gameObject.tag == "Player")
        {
            Debug.Log("Exit Area!");
            setQuestion = false;
            questionUI.SetActive(false);
        }
    }

    [SerializeField]
    private UnityEvent m_SetQuestionEvent;

    // Start is called before the first frame update
    void Start()
    {
        if (m_SetQuestionEvent == null)
            m_SetQuestionEvent = new UnityEvent();

        //Debug.Log("Ping");
    }

    /*
    private void Update()
    {
        if (setQuestion && m_SetQuestionEvent != null)
        {
            m_SetQuestionEvent.Invoke();
        }
    }
    */

    public void Ping()
    {
        Debug.Log("Ping");
    }

}
