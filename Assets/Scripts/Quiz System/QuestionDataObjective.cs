using UnityEngine;

[CreateAssetMenu(fileName = "QuestionDataObjective", menuName = "MRSM-Project-3D/QuestionDataObjective", order = 0)]
public class QuestionDataObjective : ScriptableObject
{

    [SerializeField]
    private int id;

    [SerializeField]
    private Sprite questionImage;

    [SerializeField]
    [TextArea(3, 10)]
    private string description;

    [SerializeField]
    [TextArea(3, 10)]
    private string[] answer;

    [SerializeField]
    private string correctAnswerIndex;

    public string _description
    {
        get
        {
            return description;
        }
    }

    public string[] _answer
    {
        get
        {
            return answer;
        }
    }

    public string _correctAnswerIndex
    {
        get
        {
            return correctAnswerIndex;
        }
    }
}