﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PuzzleTile : MonoBehaviour
{

    [SerializeField]
    private GameObject puzzleUI;

    [SerializeField]
    private GameObject SudokuPuzzle;

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("Enter Area!");
            puzzleUI.SetActive(true);

        }
    }

    private void OnTriggerExit(Collider other)
    {

        if (other.gameObject.tag == "Player")
        {
            Debug.Log("Exit Area!");
            puzzleUI.SetActive(false);
            SudokuPuzzle.SetActive(false);
        }
    }

    public void SudokuAccept()
    {
        SudokuPuzzle.SetActive(true);
    }

}
