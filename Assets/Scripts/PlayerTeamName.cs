using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerTeamName : MonoBehaviour
{

    [SerializeField]
    private TMP_Dropdown playerTeamNameDropdown;

    private string _playerTeamName;

    private void Start()
    {
        playerTeamNameDropdown.onValueChanged.AddListener(delegate
        {
            DropdownValueChanged(playerTeamNameDropdown);
        });
    }

    public void DropdownValueChanged(TMP_Dropdown change)
    {

        _playerTeamName = change.captionText.text;
        PlayerData.playerTeamName = change.captionText.text;
    }

    public void SelectTeam()
    {
        PlayerData.playerTeamName = _playerTeamName;
    }
}