using UnityEngine;

public class followingCam : MonoBehaviour
{
	private float turnSpeed = 4f;

	public Transform player;

	private Vector3 offset;

	private Vector3 playerPosition;

	private void Start()
	{
		offset = new Vector3(base.transform.position.x - player.position.x, base.transform.position.y - player.position.y, base.transform.position.z - player.position.z);
	}

	private void Update()
	{
		Vector3 position = player.position;
		base.transform.position = position + offset;
		if (UnityEngine.Input.GetKey(KeyCode.X))
		{
			offset = Quaternion.AngleAxis(UnityEngine.Input.GetAxis("Mouse X") * turnSpeed, Vector3.up) * offset;
			base.transform.position = player.position + offset;
			playerPosition = new Vector3(player.position.x, player.position.y + 0.245f, player.position.z);
			base.transform.LookAt(playerPosition);
		}
	}
}
