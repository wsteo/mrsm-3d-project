﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class QuestionManagerObjective : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField]
    private TMP_Text question, answerA, answerB, answerC, answerD;

    [SerializeField]
    private QuestionDataObjective[] questionsArray;

    private string selectedAnswerIndex;

    void Start()
    {
        //the index can be based on the trigger on the tiles.
        question.SetText(questionsArray[0]._description);
        answerA.SetText(questionsArray[0]._answer[0]);
        answerB.SetText(questionsArray[0]._answer[1]);
        answerC.SetText(questionsArray[0]._answer[2]);
        answerD.SetText(questionsArray[0]._answer[3]);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void checkAnswer(){
        if(selectedAnswerIndex == questionsArray[0]._correctAnswerIndex)
            Debug.Log("Correct!");
        else
            Debug.Log("Wrong!");
    }

    public void getAnswerIndex(string answerIndex){
        selectedAnswerIndex = answerIndex; 
        Debug.Log("You selected: " + selectedAnswerIndex);   
    }
}
