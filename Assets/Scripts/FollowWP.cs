﻿using System.Collections;
using UnityEngine;
using System;

public class FollowWP : MonoBehaviour
{
	[SerializeField]
	private Transform[] waypoints;

	public static int waypointIndex;

	public bool isMoving;

	public int current;

	//public Collis collide;

	private void Start(){}
	
	public void Update()
	{
		Debug.Log("Current: " + current);
		if (!isMoving)
		{
			if (base.transform.position != waypoints[current].position)
			{
				Vector3 position = Vector3.MoveTowards(base.transform.position, waypoints[current].position, 5f * Time.deltaTime);
				GetComponent<Rigidbody>().MovePosition(position);
			}
			else
			{
				current = (current + 1) % waypoints.Length;
				waypointIndex = current;
				StartCoroutine(StopMovement());
			}
		}
	}
	
	public IEnumerator StopMovement()
	{
		isMoving = true;
		UnityEngine.Debug.Log("Movement Stopped");
		yield break;
	}

	public void TriggerMovement()
	{
		isMoving = false;
		UnityEngine.Debug.Log("Moving");
	}
}
