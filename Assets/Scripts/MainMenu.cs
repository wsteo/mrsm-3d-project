﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public void PlayGame()
    {
        SceneManager.LoadScene("DifficultySelection");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void SelectJunior(){
        //TODO Player Pref save the difficulty
    }

    public void SelectSenior(){
        //TODO Player Pref save the difficulty
    }

    public void ChooseCategory(){
        SceneManager.LoadScene("TeamNameSelection");
    }

    public void PickTeamName(){
        SceneManager.LoadScene("Level01");
    }
}
