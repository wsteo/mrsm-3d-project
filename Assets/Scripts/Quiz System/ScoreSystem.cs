using UnityEngine;
using TMPro;

public class ScoreSystem : MonoBehaviour {
    public static int totalScore;

    [SerializeField]
    private TMP_Text score;

    //Temporary code for end screen
    [SerializeField]
    private TMP_Text finalEndScore;

    public int _getScore{
        get{
            return totalScore;
        }
    }
    private void Start() {
        totalScore = 0;
    }

    public void updateScore(int score){
        totalScore = totalScore + score;
    }

    public void updateScoreText(){
        score.SetText(totalScore.ToString());
        finalEndScore.SetText(totalScore.ToString());
    }

}