﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameFlow : MonoBehaviour
{
    [SerializeField]
    private GameObject player;

    [SerializeField]
    private TMP_Text teamName; 

    [SerializeField]
    private GameObject inGameUI;

    [SerializeField]
    private GameObject completeLevelUI;

    [SerializeField]
    private int endIndex = 36;

    // Update is called once per frame
    void Update()
    {
        FollowWP currentTileIndex = player.GetComponent<FollowWP>();
        if(currentTileIndex.current == endIndex){
            inGameUI.SetActive(false);
            completeLevelUI.SetActive(true);
            DisplayTeamName();
        }
    }

    public void DisplayTeamName(){
        teamName.text = PlayerData.playerTeamName.ToString();
        Debug.Log(PlayerData.playerTeamName);
    }
}
