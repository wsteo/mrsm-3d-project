using UnityEngine;

public class PlayerData : MonoBehaviour
{
    public static PlayerData instance;
    public static string playerTeamName;
    public static string playerCategory;
    public static int totalScore;

    private void Awake() {
        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }
}